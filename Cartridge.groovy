// Folders
folder( "${Application_Name}" ){

}

def sampleFolder = "${Application_Name}"
def applicationName =  "${Application_Name}"

// Jobs
def generateBuildPipelineView = sampleFolder + "/Pipeline_View_" + applicationName
def generateBuildJob = sampleFolder + "/mavenBuildJob_" + applicationName
def generateCodeAnalysisJob = sampleFolder + "/codeAnalysisJob_" + applicationName
def generateUploadToNexusJob = sampleFolder + "/uploadToNexusJob_" + applicationName
def generateDeploymentToTOmcatJob = sampleFolder + "/deploymentToTomcat_" + applicationName

// ##### GENERATE BUILD PIPELINE VIEW #####
buildPipelineView(generateBuildPipelineView) {
	title('Pipeline_View')
    displayedBuilds(5)
    selectedJob(generateBuildJob)
    alwaysAllowManualTrigger()
    showPipelineParameters()
    refreshFrequency(5)
}	
// ##### END OF BUILD PIPELINE VIEW #####

// ##### GENERATE MAVEN BUILD JOB #####
freeStyleJob(generateBuildJob) {
    scm {
        git {
            remote {
                url("http://52.53.92.227/gitlab/adopadmin/SampleWebApp.git")
            }
        }
    }
    
	wrappers {
	preBuildCleanup()
	timestamps()
    }
    
	steps {
		maven {
			mavenInstallation('maven-3.5.0')
			goals('clean')
			goals('package')
		}
    }
    
	publishers{
    	downstreamParameterized {
            trigger(generateCodeAnalysisJob) {
                condition('SUCCESS')
                parameters {
			currentBuild()
			predefinedProps([CUSTOM_WORKSPACE: '$WORKSPACE'])
                }
            }
        }
    }
}
// ##### END MAVEN BUILD JOB #####

// ##### GENERATE CODE ANALYSIS JOB #####
freeStyleJob(generateCodeAnalysisJob) {
    parameters {
        stringParam('CUSTOM_WORKSPACE', '', '')
    }
	
	customWorkspace('$CUSTOM_WORKSPACE')
    
	wrappers {
		timestamps()
    }
	
	configure { project ->
			project / 'builders' / 'hudson.plugins.sonar.SonarRunnerBuilder' {
            properties('''sonar.projectKey=hudson.plugins.sonar.SonarRunnerBuilder
                          sonar.projectName=My project
                          sonar.projectVersion=1.0
                          sonar.sources=.''')
            javaOpts()
            jdk('(Inherit From Job)')
            task()
			}
	}
    
    publishers{
    	downstreamParameterized {
            trigger(generateDeploymentToTOmcatJob) {
                condition('SUCCESS')
                parameters {
			currentBuild()
			predefinedProps([CUSTOM_WORKSPACE: '$WORKSPACE'])
                }
            }
        }
    }
}
// ##### END CODE ANALYSIS JOB #####

// ##### GENERATE DEPLOY TO TOMCAT JOB #####
freeStyleJob(generateDeploymentToTOmcatJob) {
    parameters {
        stringParam('CUSTOM_WORKSPACE', '', '')
    }
	
	customWorkspace('$CUSTOM_WORKSPACE')
    
	wrappers {
		timestamps()
    }
	
	steps {
        batchFile('''copy /y target\\*.war C:\\apache-tomcat-8.5.16\\webapps\\''')
    }
	
    publishers{
    	downstreamParameterized {
            trigger(generateUploadToNexusJob) {
                condition('SUCCESS')
                parameters {
			currentBuild()
			predefinedProps([CUSTOM_WORKSPACE: '$WORKSPACE'])
                }
            }
        }
    }
}
// ##### END DEPLOY TO TOMCAT JOB #####

// ##### GENERATE UPLOAD TO NEXUS JOB #####
freeStyleJob(generateUploadToNexusJob) {
    parameters {
        stringParam('CUSTOM_WORKSPACE', '', '')
    }
	
	customWorkspace('$CUSTOM_WORKSPACE')
    
	wrappers {
		timestamps()
    }
	
	configure { project ->
			project / 'builders' / 'sp.sd.nexusartifactuploader.NexusArtifactUploader' {
				'nexusVersion'('nexus3')
				'protocol'('http')
				'nexusUrl'('localhost:8081')
				'groupId'('sample')
				'version'('0.0.1')
				'repository'('maven-releases')
				'credentialsId'('nexus-admin')
				'artifacts'{
					'sp.sd.nexusartifactuploader.Artifact'{
						'artifactId'('sample')
						'type'('war')
						'file'('target/CounterWebApp.war')
					}
				}
			}
	}
}
// ##### END UPLOAD TO NEXUS JOB #####

